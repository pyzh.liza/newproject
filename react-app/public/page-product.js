"use strict";
setTimeout (function() {
let form = document.querySelector(".review__form");

let fullName = document.querySelector(".review__name");
let errorName = document.querySelector(".error-name");

let inputRating = document.querySelector(".review__rating");
let errorRating = document.querySelector(".error-rating");

let btnBasket = document.querySelector(".price__btn-basket");
let quantityGoods = document.querySelector(".header__number");

form.addEventListener("submit", handleSubmit);
fullName.addEventListener('focus', clearErrorName);
inputRating.addEventListener('focus', clearErrorRating);
btnBasket.addEventListener('click', handleСlick);

fullName.value = localStorage.getItem('name');
inputRating.value = localStorage.getItem('rating');
quantityGoods.innerHTML = localStorage.getItem ('quantity');
fullName.addEventListener('input', changeName); // Событие input срабатывает каждый раз при изменении значения.
inputRating.addEventListener('input', changeRating);
// btnBasket.addEventListener('click', changeQuantity);

function handleSubmit(event) {
    event.preventDefault(); // Отменяем поведение по умолчанию

    if (fullName.value.trim() === '') {
        errorName.style.visibility = "visible";
        errorName.innerHTML = 'Вы забыли указать имя и фамилию';
        return;
    }

    let nameLength = fullName.value.trim().length;
    if (nameLength <= 2) {
        errorName.style.visibility = "visible";
        errorName.innerHTML = 'Имя не может быть короче 2-хсимволов';
        return;
    }

    let rating = +inputRating.value;
    if (isNaN(rating) || rating < 1 || rating > 5) {
        errorRating.style.visibility = "visible";
        errorRating.innerHTML = 'Оценка должна быть от 1 до 5';
        return;
    }

    // Валидация пройдена, очистка формы и полей
    form.reset();
    localStorage.removeItem('name'); // удалить данные с ключом
    localStorage.removeItem('rating');
    console.log("OK");
};

function clearErrorName() {
    errorName.style.visibility = "hidden";
    errorName.innerHTML = '';
};

function clearErrorRating() {
    errorRating.style.visibility = "hidden"
    errorRating.innerHTML = '';
};

function changeName() {
    localStorage.setItem('name', fullName.value); // сохранить пару ключ/значение.
};

function changeRating() {
    localStorage.setItem('rating', inputRating.value);
};



// Добавление в корзину
function reboot () {
    if (localStorage.getItem ('quantity') > 0) {
        btnBasket.innerHTML = 'Товар уже в корзине';
        quantityGoods.style.visibility = "visible";
        btnBasket.style.background = '#888888';
    } else {
        btnBasket.innerHTML = 'Добавить в корзину';
        quantityGoods.style.visibility = "hidden";
    }
};
window.onload = reboot; // Работает, разобраться с useEffect

function handleСlick() {
    if (quantityGoods.innerHTML === '') {
        btnBasket.style.background = '#888888';
        btnBasket.innerHTML = 'Товар уже в корзине';
        quantityGoods.innerHTML = '1';
        quantityGoods.style.visibility = "visible";
        localStorage.setItem('quantity', quantityGoods.innerHTML);
    } else {
        btnBasket.style.background = '#F36223';
        btnBasket.innerHTML = 'Добавить в корзину';
        quantityGoods.innerHTML = '';
        quantityGoods.style.visibility = "hidden";
        localStorage.setItem('quantity', quantityGoods.innerHTML);
    }
};

// function changeQuantity() {
//     localStorage.setItem('quantity', quantityGoods.innerHTML);
// };
}, 500);
