import './breadCrumbs.css';

function BreadCrumbs() {
    return (
        <nav className="bread-crumbs">
           <a className="link" href="index.html">Электроника</a> {'>'}
            <a className="link" href="index.html">Смартфоны и гаджеты</a> {'>'}
            <a className="link" href="index.html">Мобильные телефоны</a> {'>'}
            <a className="link" href="index.html">Apple </a>
        </nav>
    );
}

export default BreadCrumbs;
