import './header.css';
import image from './favicon.svg';
import imageCart from './basket-hat.svg';

function Header() {
    return (
        <header className="header">
            <div className="header__content">
                <div className="header__project-name">
                    <img src={image} alt="Логотип проекта" className="header__logo"/>
                    <div><span className="header__name">Мой</span>Маркет</div>
                </div>
                <div className="header__basket"> 
                    <img src={imageCart} alt="Корзина" className="header__basket-img"/>
                    <div className="header__number"></div>
                </div>
            </div>
        </header>
    );
}
  
  export default Header;