import './footer.css';

function Footer() {
    return (
        <footer className="footer">
            <div className="footer__content">
                <div className="footer__copyright-text">
                    <div className="footer__copyright-name">© ООО «<span className="header__name">Мой</span>Маркет», 2018-2022</div>
                    <div>
                        Для уточнения информации звоните по номеру <a className="link" href="tel:+79000000000">+7 900 000 0000</a>,
                        <br />а предложения по сотрудничеству отправляйте на почту <a className="link" href="mailto:partner@mymarket.com">partner@mymarket.com</a>
                    </div>
                </div>
                <div className="footer__up"><br /><a className="link" href="/#up">Наверх</a></div>
            </div>
        </footer>
    );
  }
  
  export default Footer;
  