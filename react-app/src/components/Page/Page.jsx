import Header from "../Header/Header";
import BreadCrumbs from "../BreadCrumbs/BreadCrumbs";
import PageProduct from "../PageProduct/PageProduct";
import Footer from "../Footer/Footer";
import './page.css';

function Page() {
    return (
      <div className="page"> 
      <Header />
      <BreadCrumbs />
      <PageProduct />
      <Footer />
      </div>
    );
  }
  
  export default Page;
  