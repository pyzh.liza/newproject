
import './pageProduct.css';
import imageView1 from './imgProduct/image-1.webp';
import imageView2 from './imgProduct/image-2.webp';
import imageView3 from './imgProduct/image-3.webp';
import imageView4 from './imgProduct/image-4.webp';
import imageView5 from './imgProduct/image-5.webp';

import imageColor1 from './imgColor/color-1.webp';
import imageColor2 from './imgColor/color-2.webp';
import imageColor3 from './imgColor/color-3.webp';
import imageColor4 from './imgColor/color-4.webp';
import imageColor5 from './imgColor/color-5.webp';
import imageColor6 from './imgColor/color-6.webp';

import author1 from './photoAuthor/review-1.jpeg';
import author2 from './photoAuthor/review-2.jpeg';

import rating1 from './star-5.png';
import rating2 from './star-4.png';
import Iframe from '../Iframe/Iframe';

function PageProduct() {
    return (
        <main className="main">
            <div className="product-name">Смартфон Apple iPhone 13, синий</div>

            <div className="product-view">
                <img src={imageView1} alt="Apple iPhone 13" className="product-view__photo" />
                <img src={imageView2} alt="Apple iPhone 13 дисплей" className="product-view__photo" />
                <img src={imageView3} alt="Apple iPhone 13 вид сбоку" className="product-view__photo" />
                <img src={imageView4} alt="Apple iPhone 13 фотокамера" className="product-view__photo" />
                <img src={imageView5} alt="Apple iPhone 13 корпус" className="product-view__photo" />
            </div>

            <div className="main-unit">

                <div className="description">
                    <div className="color-product">
                        <div className="subtitle">Цвет товара: Синий</div>
                        <div className="color-product__options">
                            <button className="color-product__btn">
                                <img src={imageColor1} alt="Apple iPhone 13 (Цвет: Красный)" className="color-product__icon" />
                            </button>
                            <button className="color-product__btn">
                                <img src={imageColor2} alt="Apple iPhone 13 (Цвет: Альпийский зеленый)" className="color-product__icon" />
                            </button>
                            <button className="color-product__btn">
                                <img src={imageColor3} alt="Apple iPhone 13 (Цвет: Розовый)" className="color-product__icon" />
                            </button>
                            <button className="color-product__btn color-product__btn_selected">
                                <img src={imageColor4} alt="Apple iPhone 13 (Цвет: Синий)" className="color-product__icon" />
                            </button>
                            <button className="color-product__btn">
                                <img src={imageColor5} alt="Apple iPhone 13 (Цвет: Сияющая звезда)" className="color-product__icon" />
                            </button>
                            <button className="color-product__btn">
                                <img src={imageColor6} alt="Apple iPhone 13 (Цвет: Тёмная ночь)" className="color-product__icon" />
                            </button>
                        </div>
                    </div>

                    <div className="memory">
                        <div className="subtitle">Конфигурация памяти: 128 ГБ</div>
                        <div className="memory__options">
                            <button className="memory__btn memory__btn_selected">128 ГБ</button>
                            <button className="memory__btn">256 ГБ</button>
                            <button className="memory__btn">512 ГБ</button>
                        </div>
                    </div>

                    <div className="characteristic">
                        <div className="subtitle">Характеристики товара</div>
                        <div className="characteristic__list">
                            <ul className="characteristic__elements">
                                <li className="characteristic__element-li"><span className="text">Экран: <b>6.1</b></span></li>
                                <li className="characteristic__element-li"><span className="text">Встроенная память: <b>128 ГБ</b></span></li>
                                <li className="characteristic__element-li"><span className="text">Операционная система: <b>iOS 15</b></span></li>
                                <li className="characteristic__element-li"><span className="text">Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b></span></li>
                                <li className="characteristic__element-li"><span className="text">Процессор: <a className="link" href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank">Apple A15 Bionic</a></span></li>
                                <li className="characteristic__element-li"><span className="text">Вес: <b>173 г</b></span></li>
                            </ul>
                        </div>
                    </div>

                    <div className="product-description">
                        <div className="subtitle">Описание</div>
                        <div className="product-description__text">
                            <div>
                                Наша самая совершенная система двух камер.
                                <br />Особый взгляд на прочность дисплея.
                                <br />Чип, с которым всё супербыстрое.
                                <br />Аккумулятор держится заметно дольше.
                                <br /><i>iPhone 13 - сильный мира всего.</i>
                            </div>
                            <div>
                                Мы разработали совершенно новую схему расположения и развернулиобъективы на 45 градусов.
                                Благодаря этому внутри корпуса поместилась нашалучшая система двух камер с увеличенной
                                матрицей широкоугольной камеры. Кроме того, мы освободили место для системы оптической
                                стабилизацииизображения сдвигом матрицы. И повысили скорость работы матрицы насверхширокоугольной камере.
                            </div>
                            <div>
                                Новая сверхширокоугольная камера видит больше деталей в тёмных областяхснимков.
                                Новая широкоугольная камера улавливает на 47% больше света для более качественных фотографий и видео.
                                Новая оптическая стабилизация сосдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.
                            </div>
                            <div>
                                Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещенияфокуса и изменения резкости.
                                Просто начните запись видео. Режим «Киноэффект»будет удерживать фокус на объекте съёмки, создавая красивый эффект размытиявокруг него.
                                Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другогочеловека или объект, который появился в кадре.
                                Теперь ваши видео будут смотретьсякак настоящее кино.
                            </div>
                        </div>
                    </div>

                    <div className="comparison-models">
                        <div className="subtitle">Сравнение моделей</div>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th className="table__header">Модель</th>
                                    <th className="table__header">Вес</th>
                                    <th className="table__header">Высота</th>
                                    <th className="table__header">Ширина</th>
                                    <th className="table__header">Толщина</th>
                                    <th className="table__header">Чип</th>
                                    <th className="table__header">Объём памяти</th>
                                    {/* <th>Беспроводная зарядка</th> */}
                                    <th className="table__header">Аккумулятор</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr className="table__line">
                                    <td className="table__element">Iphone11</td>
                                    <td className="table__element">194 грамма</td>
                                    <td className="table__element">150.9 мм</td>
                                    <td className="table__element">75.7 мм</td>
                                    <td className="table__element">8.3 мм</td>
                                    <td className="table__element">A13 Bionicchip</td>
                                    <td className="table__element">до 128 Гб</td>
                                    {/* <td>Нет</td>  */}
                                    <td className="table__element">До 17 часов</td>
                                </tr>
                                <tr className="table__line">
                                    <td className="table__element">Iphone12</td>
                                    <td className="table__element">164 грамма</td>
                                    <td className="table__element">146.7 мм</td>
                                    <td className="table__element">71.5 мм</td>
                                    <td className="table__element">7.4 мм</td>
                                    <td className="table__element">A14 Bionicchip</td>
                                    <td className="table__element">до 256 Гб</td>
                                    {/* <td>Да</td> */}
                                    <td className="table__element">До 19 часов</td>
                                </tr>
                                <tr className="table__line">
                                    <td className="table__element">Iphone13</td>
                                    <td className="table__element">174 грамма</td>
                                    <td className="table__element">146.7 мм</td>
                                    <td className="table__element">71.5 мм</td>
                                    <td className="table__element">7.65 мм</td>
                                    <td className="table__element">A15 Bionicchip</td>
                                    <td className="table__element">до 512 Гб</td>
                                    {/* <td>Да</td> */}
                                    <td className="table__element">До 19 часов</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div className="sidebar">
                    <div className="price">
                        <div className="price__product">
                            <div className="price__block">
                                <div className="price__old">
                                    <div className="price__old-text">75 990₽</div>
                                    <div className="price__discount">
                                        <div className="price__discount-text">8%</div>
                                    </div>
                                </div>
                                <div className="price__actual">67 990₽</div>
                            </div>

                            <div className="price__btn-favorite"></div>
                        </div>

                        <div className="price__delivery">
                            <div>Самовывоз в четверг, 1 сентября – <b>бесплатно</b></div>
                            <div>Курьером в четверг, 1 сентября – <b>бесплатно</b></div>
                        </div>

                        <button className="price__btn-basket">Добавить в корзину</button>

                    </div>

                    <div className="advertising">
                        <div className="advertising__text">Реклама</div>
                        <div className="advertising__iframe">
                            <div className="advertising__iframe-view">
                                <Iframe />
                            </div>
                            <div className="advertising__iframe-view">
                                <Iframe />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="review">
                <div className="review__header">
                    <div className="review__header-content">
                        <div>Отзывы</div>
                        <span className="review__header-amount">425</span>
                    </div>
                </div>

                <div className="review__description">
                    <div className="review__content">
                        <img src={author1} alt="Фотография" className="review__author-photo" />
                        <div className="review__text">
                            <div className="review__author">
                                <div className="review__author-name">Марк Г.</div>
                                <img src={rating1} alt="Рейтинг (звезды)" className="review__rating-img" />
                            </div>
                            <div className="review__info review__info-text">
                                <div><span className="review__info-subtitle">Опыт использования: </span>менее месяца</div>
                                <div>
                                    <span className="review__info-subtitle">Достоинства:</span>
                                    <br />это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво.
                                    довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.
                                </div>
                                <div>
                                    <span className="review__info-subtitle">Недостатки:</span><br />к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь, а если нужно переносить фото с компа,
                                    то это только через itunes, который урезает качество фотографий исходное
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="review__separator"><div className="review__line"></div></div>

                    <div className="review__content">
                        <img src={author2} alt="Фотография" className="review__author-photo" />
                        <div className="review__text">
                            <div className="review__author">
                                <div className="review__author-name">Валерий Коваленко</div>
                                <img src={rating2} alt="Рейтинг (звезды)" className="review__rating-img" />
                            </div>
                            <div className="review__info review__info-text">
                                <div><span className="review__info-subtitle">Опыт использования: </span>менее месяца</div>
                                <div><span className="review__info-subtitle">Достоинства:</span><br />OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго</div>
                                <div><span className="review__info-subtitle">Недостатки:</span><br />Плохая ремонтопригодность</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="review__input-form">
                    <div className="subtitle">Добавить свой отзыв</div>
                    <form className="review__form">
                        <div className="review__string-form">
                            <div className="review__field-name">
                                <input type="text" name="name" placeholder="Имя и фамилия" className="review__name" />
                                <div className="error-name"></div>
                            </div>
                            <div className="review__field-rating">
                                <input type="text" name="rating" placeholder="Оценка" className="review__rating" />
                                <div className="error-rating"></div>
                            </div>
                        </div>
                        <div className="review__string-form">
                            <textarea name="text" placeholder="Текст отзыва" className="review__text-author"></textarea>
                        </div>
                        <button type="submit" className="review__btn">Отправить отзыв</button>
                    </form>
                </div>
            </div>
        </main>
    );
}
export default PageProduct;